OtRx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:OTRX:FLEXible
	single: ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:OTRX

.. code-block:: python

	ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:OTRX:FLEXible
	ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:OTRX



.. autoclass:: RsCmwBluetoothSig.Implementations.Route_.Scenario_.OtRx.OtRx
	:members:
	:undoc-members:
	:noindex: