Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:STATe:ALL:BEDR

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:STATe:ALL:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Ber_.State_.All_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: