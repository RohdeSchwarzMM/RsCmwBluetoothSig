Sing
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Sing.Sing
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.sing.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Sing_Mindex.rst
	Configure_RfSettings_Dtx_Sing_StError.rst
	Configure_RfSettings_Dtx_Sing_Fdrift.rst
	Configure_RfSettings_Dtx_Sing_Foffset.rst