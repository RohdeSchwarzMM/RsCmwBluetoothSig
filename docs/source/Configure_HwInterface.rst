HwInterface<HardwareIntf>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Intf1 .. Intf4
	rc = driver.configure.hwInterface.repcap_hardwareIntf_get()
	driver.configure.hwInterface.repcap_hardwareIntf_set(repcap.HardwareIntf.Intf1)



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:HWINterface<HardwareIntf>

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:HWINterface<HardwareIntf>



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.HwInterface.HwInterface
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.hwInterface.clone()