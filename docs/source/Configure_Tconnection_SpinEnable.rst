SpinEnable
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:SPINenable:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:SPINenable:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.SpinEnable.SpinEnable
	:members:
	:undoc-members:
	:noindex: