Le2M
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A0Reference
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A1NReference
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A2NReference
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A3NReference

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A0Reference
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A1NReference
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A2NReference
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LIMit:LENergy:LE2M:A3NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.IqCoherency_.Limit_.LowEnergy_.Le2M.Le2M
	:members:
	:undoc-members:
	:noindex: