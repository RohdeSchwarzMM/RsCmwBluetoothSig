HciCustom
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling<Instance>:HCICustom:SEND

.. code-block:: python

	CALL:BLUetooth:SIGNaling<Instance>:HCICustom:SEND



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.HciCustom.HciCustom
	:members:
	:undoc-members:
	:noindex: