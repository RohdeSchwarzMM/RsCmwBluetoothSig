Packets
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Connection_.Packets.Packets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.connection.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Connection_Packets_EpLength.rst