PacketLength
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:BRATe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:EDRate

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:BRATe
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:EDRate



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.PacketLength.PacketLength
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packets.packetLength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packets_PacketLength_LowEnergy.rst