PacketLength
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Packets_.PacketLength.PacketLength
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tconnection.packets.packetLength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tconnection_Packets_PacketLength_LowEnergy.rst