Pattern
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:BRATe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:EDRate

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:BRATe
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:EDRate



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Pattern.Pattern
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packets.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packets_Pattern_LowEnergy.rst