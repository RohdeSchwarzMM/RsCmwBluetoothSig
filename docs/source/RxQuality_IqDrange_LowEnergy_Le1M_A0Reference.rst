A0Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A0Reference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqDrange_.LowEnergy_.Le1M_.A0Reference.A0Reference
	:members:
	:undoc-members:
	:noindex: