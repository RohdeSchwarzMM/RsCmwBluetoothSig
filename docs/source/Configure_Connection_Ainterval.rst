Ainterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AINTerval:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AINTerval:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Ainterval.Ainterval
	:members:
	:undoc-members:
	:noindex: