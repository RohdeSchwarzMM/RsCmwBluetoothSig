Nmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:NMODe:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:NMODe:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search_.Step_.Nmode.Nmode
	:members:
	:undoc-members:
	:noindex: