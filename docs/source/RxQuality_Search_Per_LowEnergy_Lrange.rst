Lrange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per_.LowEnergy_.Lrange.Lrange
	:members:
	:undoc-members:
	:noindex: