Phy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PHY:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PHY:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Phy.Phy
	:members:
	:undoc-members:
	:noindex: