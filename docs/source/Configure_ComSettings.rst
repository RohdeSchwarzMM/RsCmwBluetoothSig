ComSettings<CommSettings>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Hw1 .. Hw4
	rc = driver.configure.comSettings.repcap_commSettings_get()
	driver.configure.comSettings.repcap_commSettings_set(repcap.CommSettings.Hw1)





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings.ComSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.comSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_ComSettings_StopBits.rst
	Configure_ComSettings_Parity.rst
	Configure_ComSettings_Dbits.rst
	Configure_ComSettings_ComPort.rst
	Configure_ComSettings_BaudRate.rst
	Configure_ComSettings_Protocol.rst
	Configure_ComSettings_Ereset.rst
	Configure_ComSettings_Ports.rst