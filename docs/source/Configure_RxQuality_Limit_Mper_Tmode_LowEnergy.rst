LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MPER:TMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Limit_.Mper_.Tmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: