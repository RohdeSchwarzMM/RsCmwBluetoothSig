Per
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER
	single: STOP:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER
	single: ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER

.. code-block:: python

	INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER
	STOP:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER
	ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per.Per
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.search.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Search_Per_State.rst
	RxQuality_Search_Per_LowEnergy.rst
	RxQuality_Search_Per_Nmode.rst
	RxQuality_Search_Per_Tmode.rst