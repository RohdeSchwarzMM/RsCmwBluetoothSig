Mper
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Limit_.Mper.Mper
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.limit.mper.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Limit_Mper_LowEnergy.rst
	Configure_RxQuality_Limit_Mper_Tmode.rst
	Configure_RxQuality_Limit_Mper_Nmode.rst