UsbSettings<UsbSettings>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Sett1 .. Sett4
	rc = driver.configure.usbSettings.repcap_usbSettings_get()
	driver.configure.usbSettings.repcap_usbSettings_set(repcap.UsbSettings.Sett1)





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.UsbSettings.UsbSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.usbSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_UsbSettings_UsbDevice.rst
	Configure_UsbSettings_Devices.rst