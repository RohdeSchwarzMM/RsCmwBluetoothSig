LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FOFFset:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Stab_.Foffset_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: