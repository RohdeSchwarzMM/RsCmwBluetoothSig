Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACONnect

.. code-block:: python

	CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACONnect



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Connection_Check.rst
	Call_Connection_Action.rst