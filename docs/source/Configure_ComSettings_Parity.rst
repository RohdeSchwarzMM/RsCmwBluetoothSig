Parity
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:PARity

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:PARity



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.Parity.Parity
	:members:
	:undoc-members:
	:noindex: