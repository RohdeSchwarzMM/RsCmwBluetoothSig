RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Values (4x):
	Inst1 | Inst2 | Inst3 | Inst4

CommSettings
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CommSettings.Hw1
	# Values (4x):
	Hw1 | Hw2 | Hw3 | Hw4

HardwareIntf
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.HardwareIntf.Intf1
	# Values (4x):
	Intf1 | Intf2 | Intf3 | Intf4

UsbSettings
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UsbSettings.Sett1
	# Values (4x):
	Sett1 | Sett2 | Sett3 | Sett4

