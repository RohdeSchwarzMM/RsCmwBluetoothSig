Debug
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SUALog
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SUAFswlog
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SIMulation
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:HCIWindow
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:APPWindow
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:ATTRibwindow

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SUALog
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SUAFswlog
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:SIMulation
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:HCIWindow
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:APPWindow
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:ATTRibwindow



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Debug.Debug
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.debug.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Debug_LinkLayer.rst