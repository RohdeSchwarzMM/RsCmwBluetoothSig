Packets
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:BEDR

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search_.Packets.Packets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.search.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Search_Packets_LowEnergy.rst
	Configure_RxQuality_Search_Packets_Tmode.rst
	Configure_RxQuality_Search_Packets_Nmode.rst