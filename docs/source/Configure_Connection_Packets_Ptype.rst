Ptype
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:SCO
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:ESCO
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:BRATe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:EDRate

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:SCO
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:ESCO
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:BRATe
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:EDRate



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Ptype.Ptype
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packets.ptype.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packets_Ptype_LowEnergy.rst