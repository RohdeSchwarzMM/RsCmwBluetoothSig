Audio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:LINFo

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:LINFo



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Connection_.Audio.Audio
	:members:
	:undoc-members:
	:noindex: