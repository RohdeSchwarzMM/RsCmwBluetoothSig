LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:FEC:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:FEC:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Fec_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: