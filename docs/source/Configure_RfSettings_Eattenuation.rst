Eattenuation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:EATTenuation:INPut

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:EATTenuation:OUTPut
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:EATTenuation:INPut



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Eattenuation.Eattenuation
	:members:
	:undoc-members:
	:noindex: