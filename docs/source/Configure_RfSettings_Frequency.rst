Frequency
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:TMODe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:DTMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:TXTest
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:LOOPback

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:TMODe
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:DTMode
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:TXTest
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:FREQuency:LOOPback



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Frequency.Frequency
	:members:
	:undoc-members:
	:noindex: