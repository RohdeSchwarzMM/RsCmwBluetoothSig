LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LE1M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:NMODe:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Sing_.Foffset_.Nmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: