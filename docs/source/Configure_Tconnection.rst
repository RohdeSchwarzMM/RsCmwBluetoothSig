Tconnection
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection.Tconnection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tconnection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tconnection_Interval.rst
	Configure_Tconnection_SpinEnable.rst
	Configure_Tconnection_PinCode.rst
	Configure_Tconnection_Packets.rst
	Configure_Tconnection_Phy.rst
	Configure_Tconnection_Fec.rst