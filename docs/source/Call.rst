Call
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Call.Call
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_HciCustom.rst
	Call_Rdevices.rst
	Call_DtMode.rst
	Call_LowEnergy.rst
	Call_Connection.rst