PcMode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:PCMode:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:PCMode:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.PowerControl_.PcMode.PcMode
	:members:
	:undoc-members:
	:noindex: