Tmode
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Limit_.Mper_.Tmode.Tmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.limit.mper.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Limit_Mper_Tmode_LowEnergy.rst