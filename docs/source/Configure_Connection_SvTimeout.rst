SvTimeout
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SVTimeout:LESignaling
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SVTimeout

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SVTimeout:LESignaling
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SVTimeout



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.SvTimeout.SvTimeout
	:members:
	:undoc-members:
	:noindex: