Ber
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.search.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Search_Ber_Bedr.rst
	RxQuality_Search_Ber_State.rst