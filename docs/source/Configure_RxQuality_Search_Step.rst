Step
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:BREDr
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:BREDr
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:STEP:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search_.Step.Step
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.search.step.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Search_Step_Tmode.rst
	Configure_RxQuality_Search_Step_Nmode.rst