LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LE1M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Rintegrity_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: