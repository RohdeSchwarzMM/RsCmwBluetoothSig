Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:STATe:ALL:BEDR

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:STATe:ALL:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Ber_.State_.All_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: