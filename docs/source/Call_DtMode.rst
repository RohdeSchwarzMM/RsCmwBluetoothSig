DtMode
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.DtMode.DtMode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.dtMode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_DtMode_EndTx.rst
	Call_DtMode_StartTx.rst