SynWord
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SYNWord:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SYNWord:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.SynWord.SynWord
	:members:
	:undoc-members:
	:noindex: