LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PTYPe:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Ptype_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: