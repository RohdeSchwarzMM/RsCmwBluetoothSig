A1Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE1M:A1NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqDrange_.LowEnergy_.Le1M_.A1Nreference.A1Nreference
	:members:
	:undoc-members:
	:noindex: