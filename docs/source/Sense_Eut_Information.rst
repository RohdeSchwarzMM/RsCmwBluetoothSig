Information
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:BDADdress
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:CLASs
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:COMPany
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:NAME
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:VERSion
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:LESignaling

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:BDADdress
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:CLASs
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:COMPany
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:NAME
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:VERSion
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:INFormation:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Eut_.Information.Information
	:members:
	:undoc-members:
	:noindex: