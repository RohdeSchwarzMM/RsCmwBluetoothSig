EndTx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling:DTMode:ENDTx

.. code-block:: python

	CALL:BLUetooth:SIGNaling:DTMode:ENDTx



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.DtMode_.EndTx.EndTx
	:members:
	:undoc-members:
	:noindex: