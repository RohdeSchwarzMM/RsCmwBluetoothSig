Goffset
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Goffset.Goffset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.goffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Goffset_Cte.rst