StopBits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:STOPbits

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:STOPbits



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.StopBits.StopBits
	:members:
	:undoc-members:
	:noindex: