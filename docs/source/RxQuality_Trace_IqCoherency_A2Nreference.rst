A2Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A2NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A2NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A2NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A2NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Trace_.IqCoherency_.A2Nreference.A2Nreference
	:members:
	:undoc-members:
	:noindex: