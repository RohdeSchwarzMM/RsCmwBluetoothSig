RsCmwBluetoothSig API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwBluetoothSig('TCPIP::192.168.2.101::HISLIP')
	# Instance range: Inst1 .. Inst4
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwBluetoothSig.RsCmwBluetoothSig
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	Diagnostic.rst
	Sense.rst
	Connection.rst
	Source.rst
	Route.rst
	Call.rst
	LowEnergy.rst
	RxQuality.rst
	Clean.rst