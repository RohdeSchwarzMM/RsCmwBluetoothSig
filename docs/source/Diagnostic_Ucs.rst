Ucs
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:STATe
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:FREQuency
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:MODE
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:TESTvector

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:STATe
	DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:FREQuency
	DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:MODE
	DIAGnostic:BLUetooth:SIGNaling<Instance>:UCS:TESTvector



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Ucs.Ucs
	:members:
	:undoc-members:
	:noindex: