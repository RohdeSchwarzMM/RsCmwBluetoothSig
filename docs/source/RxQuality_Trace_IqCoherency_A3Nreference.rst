A3Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A3NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A3NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A3NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A3NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Trace_.IqCoherency_.A3Nreference.A3Nreference
	:members:
	:undoc-members:
	:noindex: