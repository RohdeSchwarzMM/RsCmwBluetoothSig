LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:GOFFset:CTE:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:GOFFset:CTE:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:GOFFset:CTE:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:GOFFset:CTE:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Goffset_.Cte_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: