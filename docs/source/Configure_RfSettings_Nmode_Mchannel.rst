Mchannel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:NMODe:MCHannel:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:NMODe:MCHannel:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Nmode_.Mchannel.Mchannel
	:members:
	:undoc-members:
	:noindex: