Enums
=========

AddressType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressType.PUBLic
	# All values (2x):
	PUBLic | RANDom

AddressTypeExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AddressTypeExt.PIDentity
	# All values (4x):
	PIDentity | PUBLic | RANDom | RSIDentity

AfHopingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AfHopingMode.EUT
	# All values (3x):
	EUT | NORM | USER

AllocMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AllocMethod.LOUDness
	# All values (2x):
	LOUDness | SNR

AudioChannelMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioChannelMode.DUAL
	# All values (4x):
	DUAL | JSTereo | MONO | STEReo

AudioCodec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioCodec.SBC
	# All values (1x):
	SBC

BaudRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BaudRate.B110
	# Last value:
	value = enums.BaudRate.B96K
	# All values (24x):
	B110 | B115k | B12K | B14K | B19K | B1M | B1M5 | B234k
	B24K | B28K | B2M | B300 | B38K | B3M | B3M5 | B460k
	B48K | B4M | B500k | B576k | B57K | B600 | B921k | B96K

BbBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BbBoard.BBR1
	# Last value:
	value = enums.BbBoard.SUW44
	# All values (140x):
	BBR1 | BBR11 | BBR12 | BBR13 | BBR14 | BBR2 | BBR21 | BBR22
	BBR23 | BBR24 | BBR3 | BBR31 | BBR32 | BBR33 | BBR34 | BBR4
	BBR41 | BBR42 | BBR43 | BBR44 | BBT1 | BBT11 | BBT12 | BBT13
	BBT14 | BBT2 | BBT21 | BBT22 | BBT23 | BBT24 | BBT3 | BBT31
	BBT32 | BBT33 | BBT34 | BBT4 | BBT41 | BBT42 | BBT43 | BBT44
	SUA012 | SUA034 | SUA056 | SUA078 | SUA1 | SUA11 | SUA112 | SUA12
	SUA13 | SUA134 | SUA14 | SUA15 | SUA156 | SUA16 | SUA17 | SUA178
	SUA18 | SUA2 | SUA21 | SUA212 | SUA22 | SUA23 | SUA234 | SUA24
	SUA25 | SUA256 | SUA26 | SUA27 | SUA278 | SUA28 | SUA3 | SUA31
	SUA312 | SUA32 | SUA33 | SUA334 | SUA34 | SUA35 | SUA356 | SUA36
	SUA37 | SUA378 | SUA38 | SUA4 | SUA41 | SUA412 | SUA42 | SUA43
	SUA434 | SUA44 | SUA45 | SUA456 | SUA46 | SUA47 | SUA478 | SUA48
	SUA5 | SUA6 | SUA7 | SUA8 | SUU1 | SUU11 | SUU12 | SUU13
	SUU14 | SUU2 | SUU21 | SUU22 | SUU23 | SUU24 | SUU3 | SUU31
	SUU32 | SUU33 | SUU34 | SUU4 | SUU41 | SUU42 | SUU43 | SUU44
	SUW1 | SUW11 | SUW12 | SUW13 | SUW14 | SUW2 | SUW21 | SUW22
	SUW23 | SUW24 | SUW3 | SUW31 | SUW32 | SUW33 | SUW34 | SUW4
	SUW41 | SUW42 | SUW43 | SUW44

BlockLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BlockLength.BL12
	# All values (4x):
	BL12 | BL16 | BL4 | BL8

BrPacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BrPacketType.DH1
	# All values (3x):
	DH1 | DH3 | DH5

BurstType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BurstType.BR
	# All values (3x):
	BR | EDR | LE

CodingScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingScheme.S2
	# All values (2x):
	S2 | S8

CommProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CommProtocol.HCI
	# All values (2x):
	HCI | TWO

ConnectAction
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ConnectAction.ADConnect
	# Last value:
	value = enums.ConnectAction.TMConnect
	# All values (21x):
	ADConnect | ADENter | ADEXit | AGConnect | AUDConnect | CONNect | DETach | EMConnect
	ENAGate | ENEMode | ENHFp | EXAGate | EXEMode | EXHFp | HFPConnect | INQuire
	REController | SCONnecting | SINQuiry | STMode | TMConnect

ConnectionActionLe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionActionLe.CONNect
	# All values (6x):
	CONNect | DETach | INQuire | SCONnecting | SINQuiry | TMConnect

ConnectionState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ConnectionState.A2CNnecting
	# Last value:
	value = enums.ConnectionState.XHASmode
	# All values (47x):
	A2CNnecting | A2Connected | A2Detaching | A2SCnnected | A2SDetaching | A2SNnecting | ACNNecting | ACONected
	AENMode | AEXMode | AGCNnecting | AGConnected | CHASmode | CNASmode | CNNecting | CONNected
	DETaching | DHASmode | ECNNecting | ECONected | ECRunning | EHASmode | ENAGmode | ENEMode
	ENHFp | ENHSmode | EXAGmode | EXEMode | EXHFp | EXHSmode | HFCNnecting | HFConnected
	HSCNnecting | HSConnected | HSDetaching | INQuiring | OFF | SBY | SCONnecting | SINQuiry
	SMCNnecting | SMConnected | SMDetaching | SMIDle | TCNNecting | TCONected | XHASmode

ConTestResult
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConTestResult.FAIL
	# All values (4x):
	FAIL | NRUN | PASS | TOUT

CteType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CteType.AOA1us
	# All values (4x):
	AOA1us | AOA2us | AOD1us | AOD2us

DataBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataBits.D7
	# All values (2x):
	D7 | D8

DriftRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DriftRate.HDRF
	# All values (2x):
	HDRF | LDRF

DtxMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtxMode.SINGle
	# All values (2x):
	SINGle | SPEC

EdrPacketType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EdrPacketType.E21P
	# All values (6x):
	E21P | E23P | E25P | E31P | E33P | E35P

EutState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EutState.FAIL
	# All values (2x):
	FAIL | OK

HwInterface
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HwInterface.NONE
	# All values (3x):
	NONE | RS232 | USB

LeDiagState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeDiagState.LOADingvec
	# All values (4x):
	LOADingvec | OFF | ON | VECTorloaded

LeHoppingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeHoppingMode.ALL
	# All values (2x):
	ALL | CH2

LePacketType2
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePacketType2.RFCTe
	# All values (2x):
	RFCTe | RFPHytest

LePhysicalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LePhysicalType.LE1M
	# All values (3x):
	LE1M | LE2M | LELR

LeRangePaternType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LeRangePaternType.ALL0
	# All values (6x):
	ALL0 | ALL1 | ALT | P11 | P44 | PRBS9

LeSignalingState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LeSignalingState.CMR
	# Last value:
	value = enums.LeSignalingState.TXRunning
	# All values (12x):
	CMR | IDLE | OFF | RCOM | RXRunning | SPCM | SPRX | SPTX
	STCM | STRX | STTX | TXRunning

LogCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategory.CONTinue
	# All values (4x):
	CONTinue | ERRor | INFO | WARNing

ModIndexType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModIndexType.STAB
	# All values (2x):
	STAB | STAN

OperatingMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OperatingMode.AUDio
	# All values (6x):
	AUDio | CNTest | ECMode | LETMode | PROFiles | RFTest

PacketTypeEsco
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PacketTypeEsco._2EV3
	# Last value:
	value = enums.PacketTypeEsco.HV3
	# All values (10x):
	_2EV3 | _2EV5 | _3EV3 | _3EV5 | EV3 | EV4 | EV5 | HV1
	HV2 | HV3

PacketTypeSco
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PacketTypeSco.HV1
	# All values (3x):
	HV1 | HV2 | HV3

PageScanMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PageScanMode._0X00
	# All values (4x):
	_0X00 | _0X01 | _0X02 | _0X03

PageScanPeriodMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PageScanPeriodMode.P0
	# All values (3x):
	P0 | P1 | P2

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PowerChange
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerChange.DOWN
	# All values (4x):
	DOWN | MAX | NNE | UP

PowerControl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerControl.DOWN
	# All values (3x):
	DOWN | MAX | UP

PowerControlMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerControlMode.AUTO
	# All values (2x):
	AUTO | OFF

PowerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerFlag.MAX
	# All values (3x):
	MAX | MIN | NONE

PowerMinMax
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerMinMax.CHANged
	# All values (5x):
	CHANged | MAX | MIN | NNM | NOTS

PriorityRole
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PriorityRole.MASTer
	# All values (2x):
	MASTer | SLAVe

ProfileRole
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProfileRole.ADGate
	# All values (3x):
	ADGate | ASINk | HNDFree

Protocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Protocol.CTSRts
	# All values (3x):
	CTSRts | NONE | XONXoff

PsrMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PsrMode.R0
	# All values (3x):
	R0 | R1 | R2

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (154x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IF1 | IF2 | IF3 | IQ1I | IQ3I | IQ5I | IQ7I | R11
	R11C | R12 | R12C | R12I | R13 | R13C | R14 | R14C
	R14I | R15 | R16 | R17 | R18 | R21 | R21C | R22
	R22C | R22I | R23 | R23C | R24 | R24C | R24I | R25
	R26 | R27 | R28 | R31 | R31C | R32 | R32C | R32I
	R33 | R33C | R34 | R34C | R34I | R35 | R36 | R37
	R38 | R41 | R41C | R42 | R42C | R42I | R43 | R43C
	R44 | R44C | R44I | R45 | R46 | R47 | R48 | RA1
	RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8 | RB1
	RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8 | RC1
	RC2 | RC3 | RC4 | RC5 | RC6 | RC7 | RC8 | RD1
	RD2 | RD3 | RD4 | RD5 | RD6 | RD7 | RD8 | RE1
	RE2 | RE3 | RE4 | RE5 | RE6 | RE7 | RE8 | RF1
	RF1C | RF2 | RF2C | RF2I | RF3 | RF3C | RF4 | RF4C
	RF4I | RF5 | RF5C | RF6 | RF6C | RF7 | RF8 | RFAC
	RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5 | RG6
	RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5 | RH6
	RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

SamplingFrequency
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SamplingFrequency.SF16
	# All values (4x):
	SF16 | SF32 | SF441 | SF48

SecurityMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SecurityMode.SEC2
	# All values (2x):
	SEC2 | SEC3

SequenceNumbering
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SequenceNumbering.NORM
	# All values (2x):
	NORM | TEST

SignalingCmwRole
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalingCmwRole.CENTral
	# All values (2x):
	CENTral | PERipheral

SignalingStandard
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalingStandard.CLASsic
	# All values (2x):
	CLASsic | LESignaling

SignalingState
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SignalingState.CNNecting
	# Last value:
	value = enums.SignalingState.TCONected
	# All values (9x):
	CNNecting | CONNected | CPOWer | DETaching | INQuiring | OFF | SBY | TCNNecting
	TCONected

SpeechCode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpeechCode.ALAW
	# All values (4x):
	ALAW | CVSD | MSBC | ULAW

StopBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopBits.S1
	# All values (2x):
	S1 | S2

SubBands
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubBands.SB4
	# All values (2x):
	SB4 | SB8

SymbolTimeError
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SymbolTimeError.NEG20
	# All values (3x):
	NEG20 | OFF | POS20

SymbolTimeErrorLe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SymbolTimeErrorLe.NEG50
	# All values (3x):
	NEG50 | OFF | POS50

TestMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestMode.LOOPback
	# All values (2x):
	LOOPback | TXTest

TestVector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TestVector.INITstack
	# Last value:
	value = enums.TestVector.TV9
	# All values (47x):
	INITstack | RELoadstack | TV0 | TV1 | TV10 | TV11 | TV12 | TV13
	TV14 | TV15 | TV16 | TV17 | TV18 | TV19 | TV2 | TV20
	TV21 | TV22 | TV23 | TV24 | TV25 | TV26 | TV27 | TV28
	TV29 | TV3 | TV30 | TV31 | TV32 | TV33 | TV34 | TV35
	TV36 | TV37 | TV38 | TV39 | TV4 | TV40 | TV41 | TV42
	TV43 | TV44 | TV5 | TV6 | TV7 | TV8 | TV9

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (77x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IF1 | IF2 | IF3 | IQ2O | IQ4O | IQ6O | IQ8O | R118
	R1183 | R1184 | R11C | R11O | R11O3 | R11O4 | R12C | R13C
	R13O | R14C | R214 | R218 | R21C | R21O | R22C | R23C
	R23O | R24C | R258 | R318 | R31C | R31O | R32C | R33C
	R33O | R34C | R418 | R41C | R41O | R42C | R43C | R43O
	R44C | RA18 | RB14 | RB18 | RC18 | RD18 | RE18 | RF18
	RF1C | RF1O | RF2C | RF3C | RF3O | RF4C | RF5C | RF6C
	RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

VoiceLinkType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VoiceLinkType.ESCO
	# All values (2x):
	ESCO | SCO

