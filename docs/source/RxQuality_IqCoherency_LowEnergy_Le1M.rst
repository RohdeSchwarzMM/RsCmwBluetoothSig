Le1M
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqCoherency_.LowEnergy_.Le1M.Le1M
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.iqCoherency.lowEnergy.le1M.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_IqCoherency_LowEnergy_Le1M_A0Reference.rst
	RxQuality_IqCoherency_LowEnergy_Le1M_A1Nreference.rst
	RxQuality_IqCoherency_LowEnergy_Le1M_A2Nreference.rst
	RxQuality_IqCoherency_LowEnergy_Le1M_A3Nreference.rst