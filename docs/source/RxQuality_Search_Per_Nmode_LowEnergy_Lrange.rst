Lrange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per_.Nmode_.LowEnergy_.Lrange.Lrange
	:members:
	:undoc-members:
	:noindex: