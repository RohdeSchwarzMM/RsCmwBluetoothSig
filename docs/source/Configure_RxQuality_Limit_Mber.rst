Mber
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MBER:BRATe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MBER:EDRate

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MBER:BRATe
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:LIMit:MBER:EDRate



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Limit_.Mber.Mber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.limit.mber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Limit_Mber_Tmode.rst