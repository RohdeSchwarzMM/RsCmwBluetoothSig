Protocol
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:PROTocol

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:PROTocol



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.Protocol.Protocol
	:members:
	:undoc-members:
	:noindex: