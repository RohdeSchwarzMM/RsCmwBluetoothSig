Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:DELay:PTIMeout
	single: CONFigure:BLUetooth:SIGNaling<Instance>:DELay:TMODe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:DELay:PTIMeout
	CONFigure:BLUetooth:SIGNaling<Instance>:DELay:TMODe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Delay.Delay
	:members:
	:undoc-members:
	:noindex: