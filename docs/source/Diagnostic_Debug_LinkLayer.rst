LinkLayer
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:LINKlayer:IPADdress
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:LINKlayer:PORTaddress

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:LINKlayer:IPADdress
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DEBug:LINKlayer:PORTaddress



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Debug_.LinkLayer.LinkLayer
	:members:
	:undoc-members:
	:noindex: