LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.Tmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.per.tmode.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Per_Tmode_LowEnergy_Le1M.rst
	RxQuality_Per_Tmode_LowEnergy_Le2M.rst
	RxQuality_Per_Tmode_LowEnergy_Lrange.rst