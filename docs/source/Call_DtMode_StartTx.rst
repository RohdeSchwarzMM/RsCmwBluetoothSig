StartTx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling:DTMode:STARttx

.. code-block:: python

	CALL:BLUetooth:SIGNaling:DTMode:STARttx



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.DtMode_.StartTx.StartTx
	:members:
	:undoc-members:
	:noindex: