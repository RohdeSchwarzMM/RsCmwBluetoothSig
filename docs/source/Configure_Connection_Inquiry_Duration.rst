Duration
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:DURation:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:DURation:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Inquiry_.Duration.Duration
	:members:
	:undoc-members:
	:noindex: