Pattern
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Packets_.Pattern.Pattern
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tconnection.packets.pattern.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tconnection_Packets_Pattern_LowEnergy.rst