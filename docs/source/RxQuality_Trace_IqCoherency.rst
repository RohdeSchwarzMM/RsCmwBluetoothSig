IqCoherency
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Trace_.IqCoherency.IqCoherency
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.trace.iqCoherency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Trace_IqCoherency_A0Reference.rst
	RxQuality_Trace_IqCoherency_A1Nreference.rst
	RxQuality_Trace_IqCoherency_A2Nreference.rst
	RxQuality_Trace_IqCoherency_A3Nreference.rst