LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LE1M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PACKets:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search_.Packets_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: