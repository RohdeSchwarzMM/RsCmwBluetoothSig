LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:RINTegrity:TMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Rintegrity_.Tmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: