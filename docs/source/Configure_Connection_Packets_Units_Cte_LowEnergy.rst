LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:UNITs:CTE:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:UNITs:CTE:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:UNITs:CTE:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:UNITs:CTE:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Units_.Cte_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: