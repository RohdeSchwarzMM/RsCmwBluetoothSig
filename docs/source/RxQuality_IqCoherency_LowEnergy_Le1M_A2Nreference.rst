A2Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE1M:A2NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqCoherency_.LowEnergy_.Le1M_.A2Nreference.A2Nreference
	:members:
	:undoc-members:
	:noindex: