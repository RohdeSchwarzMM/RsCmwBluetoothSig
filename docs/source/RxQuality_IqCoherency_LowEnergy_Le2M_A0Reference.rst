A0Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:LENergy:LE2M:A0Reference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqCoherency_.LowEnergy_.Le2M_.A0Reference.A0Reference
	:members:
	:undoc-members:
	:noindex: