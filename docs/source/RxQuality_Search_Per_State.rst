State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:STATe

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:STATe



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per_.State.State
	:members:
	:undoc-members:
	:noindex: