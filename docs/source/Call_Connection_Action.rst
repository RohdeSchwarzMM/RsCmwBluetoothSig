Action
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACTion:LESignaling
	single: CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACTion

.. code-block:: python

	CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACTion:LESignaling
	CALL:BLUetooth:SIGNaling<Instance>:CONNection:ACTion



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.Connection_.Action.Action
	:members:
	:undoc-members:
	:noindex: