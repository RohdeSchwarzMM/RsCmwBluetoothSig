LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PATTern:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Pattern_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: