TypePy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:ADDRess:TYPE:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:ADDRess:TYPE:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Address_.TypePy.TypePy
	:members:
	:undoc-members:
	:noindex: