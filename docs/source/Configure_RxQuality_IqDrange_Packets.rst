Packets
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.IqDrange_.Packets.Packets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.iqDrange.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_IqDrange_Packets_LowEnergy.rst