Paging
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PSRMode

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PSRMode



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Paging.Paging
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.paging.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Paging_Timeout.rst
	Configure_Connection_Paging_Ptarget.rst