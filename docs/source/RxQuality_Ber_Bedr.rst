Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	single: INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	single: STOP:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR

.. code-block:: python

	ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	STOP:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	READ:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:BER:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Ber_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: