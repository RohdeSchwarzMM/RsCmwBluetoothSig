BdAddress
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BDADdress:CMW
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BDADdress:EUT

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BDADdress:CMW
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BDADdress:EUT



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.BdAddress.BdAddress
	:members:
	:undoc-members:
	:noindex: