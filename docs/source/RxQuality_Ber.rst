Ber
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Ber.Ber
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.ber.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Ber_State.rst
	RxQuality_Ber_Bedr.rst