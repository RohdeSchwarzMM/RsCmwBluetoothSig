Swindow
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SWINdow:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SWINdow:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Swindow.Swindow
	:members:
	:undoc-members:
	:noindex: