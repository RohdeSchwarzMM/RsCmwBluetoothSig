LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:PACKets:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:PACKets:LENergy:LE1M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:PACKets:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQCoherency:PACKets:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.IqCoherency_.Packets_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: