A1Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A1NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A1NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A1NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A1NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Trace_.IqCoherency_.A1Nreference.A1Nreference
	:members:
	:undoc-members:
	:noindex: