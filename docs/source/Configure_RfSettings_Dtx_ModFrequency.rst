ModFrequency
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.ModFrequency.ModFrequency
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.modFrequency.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_ModFrequency_Nmode.rst
	Configure_RfSettings_Dtx_ModFrequency_Tmode.rst
	Configure_RfSettings_Dtx_ModFrequency_LowEnergy.rst