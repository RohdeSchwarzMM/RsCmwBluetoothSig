Mindex
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:MINDex:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:MINDex:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Sing_.Mindex.Mindex
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.sing.mindex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Sing_Mindex_Nmode.rst
	Configure_RfSettings_Dtx_Sing_Mindex_Standard.rst
	Configure_RfSettings_Dtx_Sing_Mindex_Stable.rst
	Configure_RfSettings_Dtx_Sing_Mindex_Tmode.rst
	Configure_RfSettings_Dtx_Sing_Mindex_LowEnergy.rst