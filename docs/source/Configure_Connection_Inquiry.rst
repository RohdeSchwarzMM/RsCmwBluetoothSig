Inquiry
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:ILENgth

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:ILENgth



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Inquiry.Inquiry
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.inquiry.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Inquiry_Ptargets.rst
	Configure_Connection_Inquiry_NoResponses.rst
	Configure_Connection_Inquiry_Sinterval.rst
	Configure_Connection_Inquiry_Duration.rst
	Configure_Connection_Inquiry_Swindow.rst