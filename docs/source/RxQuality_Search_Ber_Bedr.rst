Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	single: ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	single: STOP:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR

.. code-block:: python

	INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	STOP:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Ber_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: