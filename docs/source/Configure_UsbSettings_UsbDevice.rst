UsbDevice
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:USBSettings<UsbSettings>:USBDevice

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:USBSettings<UsbSettings>:USBDevice



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.UsbSettings_.UsbDevice.UsbDevice
	:members:
	:undoc-members:
	:noindex: