LowEnergy
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.lowEnergy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	LowEnergy_State.rst