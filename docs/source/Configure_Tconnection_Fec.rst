Fec
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Fec.Fec
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tconnection.fec.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tconnection_Fec_LowEnergy.rst