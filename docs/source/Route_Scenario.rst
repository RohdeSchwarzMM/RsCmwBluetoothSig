Scenario
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:STATe

.. code-block:: python

	ROUTe:BLUetooth:SIGNaling<Instance>:SCENario:STATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Route_.Scenario.Scenario
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_OtRx.rst