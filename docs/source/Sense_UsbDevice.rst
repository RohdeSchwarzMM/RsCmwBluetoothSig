UsbDevice
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.UsbDevice.UsbDevice
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.usbDevice.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UsbDevice_Information.rst