StError
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:STERror:EDRate
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:STERror:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:STERror:EDRate
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:STERror:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Stab_.StError.StError
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.stab.stError.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Stab_StError_Nmode.rst
	Configure_RfSettings_Dtx_Stab_StError_Tmode.rst
	Configure_RfSettings_Dtx_Stab_StError_LowEnergy.rst