LeSignaling
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SLATency:LESignaling:CPERipheral
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SLATency:LESignaling:CCENtral

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SLATency:LESignaling:CPERipheral
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SLATency:LESignaling:CCENtral



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Slatency_.LeSignaling.LeSignaling
	:members:
	:undoc-members:
	:noindex: