Tmode
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Packets_.Tmode.Tmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.packets.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Packets_Tmode_LowEnergy.rst