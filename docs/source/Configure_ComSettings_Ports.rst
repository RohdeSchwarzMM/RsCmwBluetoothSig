Ports
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings:PORTs:CATalog

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings:PORTs:CATalog



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.Ports.Ports
	:members:
	:undoc-members:
	:noindex: