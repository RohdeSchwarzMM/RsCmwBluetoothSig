BaudRate
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:BAUDrate

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:BAUDrate



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.BaudRate.BaudRate
	:members:
	:undoc-members:
	:noindex: