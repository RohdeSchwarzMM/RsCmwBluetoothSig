Hmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:NMODe:HMODe:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:NMODe:HMODe:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Nmode_.Hmode.Hmode
	:members:
	:undoc-members:
	:noindex: