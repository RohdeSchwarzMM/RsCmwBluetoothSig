Phy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PHY:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PHY:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Phy.Phy
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.phy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Phy_Nmode.rst