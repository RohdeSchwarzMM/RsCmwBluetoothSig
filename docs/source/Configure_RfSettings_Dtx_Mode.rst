Mode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MODE:EDRate
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MODE:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MODE:EDRate
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MODE:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Mode.Mode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.mode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Mode_Nmode.rst
	Configure_RfSettings_Dtx_Mode_Tmode.rst
	Configure_RfSettings_Dtx_Mode_LowEnergy.rst