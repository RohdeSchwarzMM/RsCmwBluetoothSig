Audio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:AUDio:PRFRole
	single: CONFigure:BLUetooth:SIGNaling<Instance>:AUDio:CMWRole

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:AUDio:PRFRole
	CONFigure:BLUetooth:SIGNaling<Instance>:AUDio:CMWRole



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Audio.Audio
	:members:
	:undoc-members:
	:noindex: