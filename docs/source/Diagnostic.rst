Diagnostic
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:WCMap

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:WCMap



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic.Diagnostic
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Delay.rst
	Diagnostic_Le.rst
	Diagnostic_Ucs.rst
	Diagnostic_Debug.rst
	Diagnostic_Connection.rst
	Diagnostic_RxQuality.rst