A0Reference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A0Reference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A0Reference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A0Reference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:TRACe:IQCoherency:A0Reference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Trace_.IqCoherency_.A0Reference.A0Reference
	:members:
	:undoc-members:
	:noindex: