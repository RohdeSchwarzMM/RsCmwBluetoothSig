Eut
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Eut.Eut
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.eut.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Eut_Capability.rst
	Sense_Eut_Information.rst
	Sense_Eut_Csettings.rst
	Sense_Eut_PowerControl.rst