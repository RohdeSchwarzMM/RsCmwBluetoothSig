LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:MOEXception:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:MOEXception:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:MOEXception:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:MOEXception:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.IqDrange_.MoException_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: