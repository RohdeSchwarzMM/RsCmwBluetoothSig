Devices
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:USBSettings:DEVices:CATalog

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:USBSettings:DEVices:CATalog



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.UsbSettings_.Devices.Devices
	:members:
	:undoc-members:
	:noindex: