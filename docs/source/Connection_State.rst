State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:CONNection:STATe

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:CONNection:STATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Connection_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.connection.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Connection_State_All.rst
	Connection_State_LeSignaling.rst