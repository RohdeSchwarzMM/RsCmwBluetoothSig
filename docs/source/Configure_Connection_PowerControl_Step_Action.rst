Action
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:STEP:ACTion:LESignaling
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:STEP:ACTion

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:STEP:ACTion:LESignaling
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:STEP:ACTion



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.PowerControl_.Step_.Action.Action
	:members:
	:undoc-members:
	:noindex: