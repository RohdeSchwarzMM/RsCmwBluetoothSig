Debug
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Debug.Debug
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.debug.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Debug_Rx.rst