Le2M
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:PER:NMODe:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per_.Nmode_.LowEnergy_.Le2M.Le2M
	:members:
	:undoc-members:
	:noindex: