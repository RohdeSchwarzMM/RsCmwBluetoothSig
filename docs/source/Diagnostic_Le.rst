Le
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:MODE
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:STATe
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:PLENgth
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:CHANnel
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:PATTern

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:MODE
	DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:STATe
	DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:PLENgth
	DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:CHANnel
	DIAGnostic:BLUetooth:SIGNaling<Instance>:LE:PATTern



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Le.Le
	:members:
	:undoc-members:
	:noindex: