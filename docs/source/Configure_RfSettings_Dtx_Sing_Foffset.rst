Foffset
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:EDRate
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:EDRate
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:SING:FOFFset:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Sing_.Foffset.Foffset
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.sing.foffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Sing_Foffset_Nmode.rst
	Configure_RfSettings_Dtx_Sing_Foffset_Tmode.rst
	Configure_RfSettings_Dtx_Sing_Foffset_LowEnergy.rst