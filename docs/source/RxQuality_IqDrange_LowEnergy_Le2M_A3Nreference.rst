A3Nreference
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:LENergy:LE2M:A3NReference



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqDrange_.LowEnergy_.Le2M_.A3Nreference.A3Nreference
	:members:
	:undoc-members:
	:noindex: