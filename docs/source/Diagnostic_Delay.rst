Delay
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DELay:PTIMeout
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:DELay:TMODe

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:DELay:PTIMeout
	DIAGnostic:BLUetooth:SIGNaling<Instance>:DELay:TMODe



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Delay.Delay
	:members:
	:undoc-members:
	:noindex: