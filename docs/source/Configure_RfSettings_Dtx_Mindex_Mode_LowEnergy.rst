LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:MINDex:MODE:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Mindex_.Mode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: