Channel
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:TMODe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:DTMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:LOOPback
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:TXTest

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:TMODe
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:DTMode
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:LOOPback
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:CHANnel:TXTest



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Channel.Channel
	:members:
	:undoc-members:
	:noindex: