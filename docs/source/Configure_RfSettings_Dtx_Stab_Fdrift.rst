Fdrift
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:EDRate
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:EDRate
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Stab_.Fdrift.Fdrift
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.stab.fdrift.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Stab_Fdrift_Nmode.rst
	Configure_RfSettings_Dtx_Stab_Fdrift_Tmode.rst
	Configure_RfSettings_Dtx_Stab_Fdrift_LowEnergy.rst