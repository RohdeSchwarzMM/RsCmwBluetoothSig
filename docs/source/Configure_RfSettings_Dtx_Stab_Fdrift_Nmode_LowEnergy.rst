LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LE1M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:FDRift:NMODe:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Stab_.Fdrift_.Nmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: