Nmode
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Per_.Nmode.Nmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.search.per.nmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Search_Per_Nmode_LowEnergy.rst