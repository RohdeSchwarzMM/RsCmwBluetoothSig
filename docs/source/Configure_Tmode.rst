Tmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TMODe:LENergy
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TMODe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TMODe:LENergy
	CONFigure:BLUetooth:SIGNaling<Instance>:TMODe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tmode.Tmode
	:members:
	:undoc-members:
	:noindex: