Information
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DPRotocol
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DSUBclass
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DCLass
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:IDPRoduct
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:IDVendor
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:PRODuct
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:SERial
	single: SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:MANufacturer

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DPRotocol
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DSUBclass
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:DCLass
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:IDPRoduct
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:IDVendor
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:PRODuct
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:SERial
	SENSe:BLUetooth:SIGNaling<Instance>:USBDevice:INFormation:MANufacturer



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.UsbDevice_.Information.Information
	:members:
	:undoc-members:
	:noindex: