Rdevices
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling<Instance>:RDEVices

.. code-block:: python

	CALL:BLUetooth:SIGNaling<Instance>:RDEVices



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.Rdevices.Rdevices
	:members:
	:undoc-members:
	:noindex: