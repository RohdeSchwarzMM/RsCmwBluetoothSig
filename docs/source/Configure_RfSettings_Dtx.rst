Dtx
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx.Dtx
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Stab.rst
	Configure_RfSettings_Dtx_Sing.rst
	Configure_RfSettings_Dtx_ModFrequency.rst
	Configure_RfSettings_Dtx_Mode.rst
	Configure_RfSettings_Dtx_Mindex.rst