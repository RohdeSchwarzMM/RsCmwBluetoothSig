Ssize
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:SSIZe:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:SSIZe:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.PowerControl_.Ssize.Ssize
	:members:
	:undoc-members:
	:noindex: