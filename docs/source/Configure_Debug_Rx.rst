Rx
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Debug_.Rx.Rx
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.debug.rx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Debug_Rx_Correlation.rst
	Configure_Debug_Rx_Trigger.rst