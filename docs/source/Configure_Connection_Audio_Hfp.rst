Hfp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:HFP:CASTartup

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:HFP:CASTartup



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Audio_.Hfp.Hfp
	:members:
	:undoc-members:
	:noindex: