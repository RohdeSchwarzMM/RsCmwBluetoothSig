LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:FEC:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:FEC:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Fec_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: