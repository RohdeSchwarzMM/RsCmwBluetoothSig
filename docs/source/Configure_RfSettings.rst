RfSettings
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ARPower
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ARANging
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ENPower
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:LEVel
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:UMARgin
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:PCONtrol
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:HOPPing

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ARPower
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ARANging
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:ENPower
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:LEVel
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:UMARgin
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:PCONtrol
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:HOPPing



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings.RfSettings
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx.rst
	Configure_RfSettings_Nmode.rst
	Configure_RfSettings_Channel.rst
	Configure_RfSettings_Frequency.rst
	Configure_RfSettings_AidOverride.rst
	Configure_RfSettings_Goffset.rst
	Configure_RfSettings_Aoffset.rst
	Configure_RfSettings_Nantenna.rst
	Configure_RfSettings_Eattenuation.rst
	Configure_RfSettings_AfHopping.rst