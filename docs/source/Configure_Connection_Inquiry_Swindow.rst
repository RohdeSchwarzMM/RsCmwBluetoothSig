Swindow
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:SWINdow:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:SWINdow:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Inquiry_.Swindow.Swindow
	:members:
	:undoc-members:
	:noindex: