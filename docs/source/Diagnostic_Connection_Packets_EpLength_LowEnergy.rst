LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: DIAGnostic:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:EPLength:LENergy:LE2M

.. code-block:: python

	DIAGnostic:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:EPLength:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Diagnostic_.Connection_.Packets_.EpLength_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: