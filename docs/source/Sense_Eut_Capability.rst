Capability
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ESCO
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:SCLass
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ENCRyption
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:PCONtrol
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:EPControl
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:PSAVing
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:CONNection
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:SCO
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ACL
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:LESignaling

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ESCO
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:SCLass
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ENCRyption
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:PCONtrol
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:EPControl
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:PSAVing
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:CONNection
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:SCO
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:ACL
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:CAPability:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Eut_.Capability.Capability
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.eut.capability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Eut_Capability_Adp.rst