Sense
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:CMAP

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:CMAP



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense.Sense
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UsbDevice.rst
	Sense_Eut.rst
	Sense_Connection.rst
	Sense_Elogging.rst