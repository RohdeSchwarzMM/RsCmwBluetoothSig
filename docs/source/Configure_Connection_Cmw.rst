Cmw
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Cmw.Cmw
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.cmw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Cmw_Role.rst