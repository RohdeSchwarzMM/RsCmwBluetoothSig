Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:STATe:BEDR

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:BER:STATe:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Ber_.State_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: