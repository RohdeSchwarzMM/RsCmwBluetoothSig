Sinterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SINTerval:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:SINTerval:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Sinterval.Sinterval
	:members:
	:undoc-members:
	:noindex: