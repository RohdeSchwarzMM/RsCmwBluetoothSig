Le1M
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M

.. code-block:: python

	READ:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:IQDRange:ANTMeanamp:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.IqDrange_.AntMeanAmp_.LowEnergy_.Le1M.Le1M
	:members:
	:undoc-members:
	:noindex: