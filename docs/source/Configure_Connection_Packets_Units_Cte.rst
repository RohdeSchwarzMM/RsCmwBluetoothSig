Cte
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.Units_.Cte.Cte
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packets.units.cte.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packets_Units_Cte_LowEnergy.rst