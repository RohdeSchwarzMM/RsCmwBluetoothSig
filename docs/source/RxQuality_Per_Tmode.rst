Tmode
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.Tmode.Tmode
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.per.tmode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Per_Tmode_LowEnergy.rst