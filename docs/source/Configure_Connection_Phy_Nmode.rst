Nmode
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PHY:NMODe:LENergy

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PHY:NMODe:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Phy_.Nmode.Nmode
	:members:
	:undoc-members:
	:noindex: