Connection
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BTYPe
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:DELay
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:WHITening

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:BTYPe
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:DELay
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:WHITening



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection.Connection
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Audio.rst
	Configure_Connection_Packets.rst
	Configure_Connection_SynWord.rst
	Configure_Connection_Cscheme.rst
	Configure_Connection_Fec.rst
	Configure_Connection_Phy.rst
	Configure_Connection_PowerControl.rst
	Configure_Connection_Paging.rst
	Configure_Connection_BdAddress.rst
	Configure_Connection_Inquiry.rst
	Configure_Connection_EutCharacter.rst
	Configure_Connection_WfcMap.rst
	Configure_Connection_Slatency.rst
	Configure_Connection_Rencryption.rst
	Configure_Connection_Iencryption.rst
	Configure_Connection_Cmw.rst
	Configure_Connection_Address.rst
	Configure_Connection_Raddress.rst
	Configure_Connection_SvTimeout.rst
	Configure_Connection_Interval.rst
	Configure_Connection_Sinterval.rst
	Configure_Connection_Ainterval.rst
	Configure_Connection_Swindow.rst
	Configure_Connection_Pperiod.rst