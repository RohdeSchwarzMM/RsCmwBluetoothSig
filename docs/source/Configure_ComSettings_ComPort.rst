ComPort
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:COMPort

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:COMPort



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.ComPort.ComPort
	:members:
	:undoc-members:
	:noindex: