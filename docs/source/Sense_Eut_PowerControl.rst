PowerControl
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Eut_.PowerControl.PowerControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.eut.powerControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Eut_PowerControl_State.rst