Sinterval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:SINTerval:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INQuiry:SINTerval:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Inquiry_.Sinterval.Sinterval
	:members:
	:undoc-members:
	:noindex: