Mindex
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:MINDex:BRATe

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:DTX:STAB:MINDex:BRATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Stab_.Mindex.Mindex
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.stab.mindex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Stab_Mindex_Nmode.rst
	Configure_RfSettings_Dtx_Stab_Mindex_Standard.rst
	Configure_RfSettings_Dtx_Stab_Mindex_Stable.rst
	Configure_RfSettings_Dtx_Stab_Mindex_Tmode.rst
	Configure_RfSettings_Dtx_Stab_Mindex_LowEnergy.rst