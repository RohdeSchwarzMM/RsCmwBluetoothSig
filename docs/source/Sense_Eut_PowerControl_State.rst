State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:LESignaling
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:GFSK
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:DQPSk
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:DPSK
	single: SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe

.. code-block:: python

	SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:LESignaling
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:GFSK
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:DQPSk
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe:DPSK
	SENSe:BLUetooth:SIGNaling<Instance>:EUT:PCONtrol:STATe



.. autoclass:: RsCmwBluetoothSig.Implementations.Sense_.Eut_.PowerControl_.State.State
	:members:
	:undoc-members:
	:noindex: