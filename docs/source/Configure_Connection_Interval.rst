Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INTerval:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:INTerval:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Interval.Interval
	:members:
	:undoc-members:
	:noindex: