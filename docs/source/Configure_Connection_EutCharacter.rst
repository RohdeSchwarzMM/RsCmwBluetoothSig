EutCharacter
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:OPCMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:SNBehaviour
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:TCPChange
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:RLSettling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:OPCMode
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:SNBehaviour
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:TCPChange
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:EUTCharacter:RLSettling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.EutCharacter.EutCharacter
	:members:
	:undoc-members:
	:noindex: