Standard
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.Dtx_.Sing_.Mindex_.Standard.Standard
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.dtx.sing.mindex.standard.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Dtx_Sing_Mindex_Standard_Tmode.rst
	Configure_RfSettings_Dtx_Sing_Mindex_Standard_LowEnergy.rst