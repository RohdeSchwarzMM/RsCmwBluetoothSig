Dbits
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:DBITs

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:COMSettings<CommSettings>:DBITs



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.ComSettings_.Dbits.Dbits
	:members:
	:undoc-members:
	:noindex: