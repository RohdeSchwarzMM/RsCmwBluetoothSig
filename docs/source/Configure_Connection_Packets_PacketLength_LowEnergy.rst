LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LRANge
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LE2M

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LRANge
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PACKets:PLENgth:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets_.PacketLength_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: