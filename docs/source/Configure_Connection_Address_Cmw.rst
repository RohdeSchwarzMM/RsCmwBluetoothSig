Cmw
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:ADDRess:CMW:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:ADDRess:CMW:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Address_.Cmw.Cmw
	:members:
	:undoc-members:
	:noindex: