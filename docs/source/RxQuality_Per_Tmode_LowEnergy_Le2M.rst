Le2M
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M

.. code-block:: python

	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M
	READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LE2M



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.Tmode_.LowEnergy_.Le2M.Le2M
	:members:
	:undoc-members:
	:noindex: