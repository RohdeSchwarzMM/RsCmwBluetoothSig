Le1M
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M
	READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M
	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:NMODe:LENergy:LE1M



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.Nmode_.LowEnergy_.Le1M.Le1M
	:members:
	:undoc-members:
	:noindex: