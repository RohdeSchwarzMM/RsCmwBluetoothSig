Lrange
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge
	single: READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge

.. code-block:: python

	CALCulate:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge
	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge
	READ:BLUetooth:SIGNaling<Instance>:RXQuality:PER:TMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.Tmode_.LowEnergy_.Lrange.Lrange
	:members:
	:undoc-members:
	:noindex: