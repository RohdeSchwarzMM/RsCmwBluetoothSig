Search
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search.Search
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.search.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Search_Per.rst
	RxQuality_Search_Ber.rst