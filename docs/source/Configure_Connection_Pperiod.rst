Pperiod
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PPERiod:MINimum
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PPERiod

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PPERiod:MINimum
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PPERiod



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Pperiod.Pperiod
	:members:
	:undoc-members:
	:noindex: