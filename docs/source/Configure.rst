Configure
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:OPMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CPRotocol
	single: CONFigure:BLUetooth:SIGNaling<Instance>:STANdard

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:OPMode
	CONFigure:BLUetooth:SIGNaling<Instance>:CPRotocol
	CONFigure:BLUetooth:SIGNaling<Instance>:STANdard



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure.Configure
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Delay.rst
	Configure_Tmode.rst
	Configure_Audio.rst
	Configure_Connection.rst
	Configure_LowEnergy.rst
	Configure_UsbSettings.rst
	Configure_ComSettings.rst
	Configure_HwInterface.rst
	Configure_Debug.rst
	Configure_Tconnection.rst
	Configure_RfSettings.rst
	Configure_RxQuality.rst