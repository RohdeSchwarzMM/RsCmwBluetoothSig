LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:LIMit:MPER:NMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search_.Limit_.Mper_.Nmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: