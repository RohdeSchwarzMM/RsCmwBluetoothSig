LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:PACKets:TMODe:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Packets_.Tmode_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: