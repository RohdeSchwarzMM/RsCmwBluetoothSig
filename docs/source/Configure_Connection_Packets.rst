Packets
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Packets.Packets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Packets_Ptype.rst
	Configure_Connection_Packets_PacketLength.rst
	Configure_Connection_Packets_Pattern.rst
	Configure_Connection_Packets_Units.rst
	Configure_Connection_Packets_TypePy.rst