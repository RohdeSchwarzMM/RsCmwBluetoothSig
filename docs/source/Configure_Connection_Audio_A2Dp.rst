A2Dp
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:ACCSlave
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:BITRate
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:MAXBitpool
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:MINBitpool
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:ALCMethod
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:SUBBands
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:BLKLength
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:CHMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:SMPFrequency
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:CODec

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:ACCSlave
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:BITRate
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:MAXBitpool
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:MINBitpool
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:ALCMethod
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:SUBBands
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:BLKLength
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:CHMode
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:SMPFrequency
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:A2DP:CODec



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Audio_.A2Dp.A2Dp
	:members:
	:undoc-members:
	:noindex: