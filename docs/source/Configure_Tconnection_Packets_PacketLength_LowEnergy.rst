LowEnergy
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LE1M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LE2M
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LRANge

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LE1M
	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LE2M
	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:PACKets:PLENgth:LENergy:LRANge



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Packets_.PacketLength_.LowEnergy.LowEnergy
	:members:
	:undoc-members:
	:noindex: