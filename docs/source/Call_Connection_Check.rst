Check
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CALL:BLUetooth:SIGNaling<Instance>:CONNection:CHECk:LENergy

.. code-block:: python

	CALL:BLUetooth:SIGNaling<Instance>:CONNection:CHECk:LENergy



.. autoclass:: RsCmwBluetoothSig.Implementations.Call_.Connection_.Check.Check
	:members:
	:undoc-members:
	:noindex: