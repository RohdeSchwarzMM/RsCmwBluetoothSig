Ptarget
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PTARget:LESignaling
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PTARget

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PTARget:LESignaling
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:PTARget



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Paging_.Ptarget.Ptarget
	:members:
	:undoc-members:
	:noindex: