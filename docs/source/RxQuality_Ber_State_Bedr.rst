Bedr
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:STATe:BEDR

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:BER:STATe:BEDR



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Ber_.State_.Bedr.Bedr
	:members:
	:undoc-members:
	:noindex: