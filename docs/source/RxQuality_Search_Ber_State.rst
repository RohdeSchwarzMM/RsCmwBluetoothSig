State
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Search_.Ber_.State.State
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.search.ber.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Search_Ber_State_All.rst
	RxQuality_Search_Ber_State_Bedr.rst