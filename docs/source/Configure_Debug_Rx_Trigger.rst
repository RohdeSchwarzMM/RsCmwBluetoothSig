Trigger
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:TRIGger:PLEVel

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:TRIGger:PLEVel



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Debug_.Rx_.Trigger.Trigger
	:members:
	:undoc-members:
	:noindex: