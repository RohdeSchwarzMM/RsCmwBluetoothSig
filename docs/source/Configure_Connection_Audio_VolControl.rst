VolControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VOLControl:MICGain
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VOLControl:SPEaker

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VOLControl:MICGain
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VOLControl:SPEaker



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Audio_.VolControl.VolControl
	:members:
	:undoc-members:
	:noindex: