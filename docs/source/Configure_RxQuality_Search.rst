Search
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:TOUT

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RXQuality:SEARch:TOUT



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RxQuality_.Search.Search
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rxQuality.search.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RxQuality_Search_Rintegrity.rst
	Configure_RxQuality_Search_Limit.rst
	Configure_RxQuality_Search_Packets.rst
	Configure_RxQuality_Search_Step.rst