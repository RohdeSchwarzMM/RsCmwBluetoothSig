Correlation
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:CORRelation:THReshold
	single: CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:CORRelation:TIMeout

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:CORRelation:THReshold
	CONFigure:BLUetooth:SIGNaling<Instance>:DEBug:RX:CORRelation:TIMeout



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Debug_.Rx_.Correlation.Correlation
	:members:
	:undoc-members:
	:noindex: