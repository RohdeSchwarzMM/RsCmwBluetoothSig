Interval
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:INTerval:LESignaling

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:TCONnection:INTerval:LESignaling



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Interval.Interval
	:members:
	:undoc-members:
	:noindex: