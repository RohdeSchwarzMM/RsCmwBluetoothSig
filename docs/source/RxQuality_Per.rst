Per
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:PER
	single: INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:PER
	single: STOP:BLUetooth:SIGNaling<Instance>:RXQuality:PER

.. code-block:: python

	ABORt:BLUetooth:SIGNaling<Instance>:RXQuality:PER
	INITiate:BLUetooth:SIGNaling<Instance>:RXQuality:PER
	STOP:BLUetooth:SIGNaling<Instance>:RXQuality:PER



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per.Per
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.rxQuality.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	RxQuality_Per_State.rst
	RxQuality_Per_LowEnergy.rst
	RxQuality_Per_Nmode.rst
	RxQuality_Per_Tmode.rst