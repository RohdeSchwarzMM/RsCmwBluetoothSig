Packets
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Tconnection_.Packets.Packets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.tconnection.packets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Tconnection_Packets_Pattern.rst
	Configure_Tconnection_Packets_PacketLength.rst