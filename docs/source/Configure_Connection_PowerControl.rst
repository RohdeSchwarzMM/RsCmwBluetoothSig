PowerControl
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:EPCMode

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PCONtrol:EPCMode



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.PowerControl.PowerControl
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.powerControl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_PowerControl_Step.rst
	Configure_Connection_PowerControl_Ssize.rst
	Configure_Connection_PowerControl_PcMode.rst