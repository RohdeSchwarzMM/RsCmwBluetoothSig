Ptargets
----------------------------------------





.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Inquiry_.Ptargets.Ptargets
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.inquiry.ptargets.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Inquiry_Ptargets_Catalog.rst