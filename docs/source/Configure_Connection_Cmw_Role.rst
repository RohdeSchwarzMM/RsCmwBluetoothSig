Role
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:CMW:ROLE:LESignaling
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:CMW:ROLE

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:CMW:ROLE:LESignaling
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:CMW:ROLE



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Cmw_.Role.Role
	:members:
	:undoc-members:
	:noindex: