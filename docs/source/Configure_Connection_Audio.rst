Audio
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:SECMode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VLINk
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:PINCode
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:CODec

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:SECMode
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:VLINk
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:PINCode
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:AUDio:CODec



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Audio.Audio
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.audio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Audio_VolControl.rst
	Configure_Connection_Audio_Hfp.rst
	Configure_Connection_Audio_A2Dp.rst