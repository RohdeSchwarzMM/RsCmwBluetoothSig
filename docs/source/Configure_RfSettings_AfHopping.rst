AfHopping
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:AFHopping:UCHannels
	single: CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:AFHopping

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:AFHopping:UCHannels
	CONFigure:BLUetooth:SIGNaling<Instance>:RFSettings:AFHopping



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.RfSettings_.AfHopping.AfHopping
	:members:
	:undoc-members:
	:noindex: