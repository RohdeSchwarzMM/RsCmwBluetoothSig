State
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:STATe

.. code-block:: python

	FETCh:BLUetooth:SIGNaling<Instance>:RXQuality:PER:STATe



.. autoclass:: RsCmwBluetoothSig.Implementations.RxQuality_.Per_.State.State
	:members:
	:undoc-members:
	:noindex: