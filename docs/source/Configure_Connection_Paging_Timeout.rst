Timeout
----------------------------------------



.. rubric:: SCPI Commands

.. index::
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:TOUT:LESignaling
	single: CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:TOUT

.. code-block:: python

	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:TOUT:LESignaling
	CONFigure:BLUetooth:SIGNaling<Instance>:CONNection:PAGing:TOUT



.. autoclass:: RsCmwBluetoothSig.Implementations.Configure_.Connection_.Paging_.Timeout.Timeout
	:members:
	:undoc-members:
	:noindex: